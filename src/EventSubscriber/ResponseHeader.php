<?php

/**
 * @file
 * Contains \Drupal\maintenance_mode_response_header\EventSubscriber\ResponseHeader.
 */

namespace Drupal\maintenance_mode_response_header\EventSubscriber;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides ResponseHeader.
 */
class ResponseHeader implements EventSubscriberInterface {

  /**
   * Sets extra HTTP headers.
   */
  public function onRespond(FilterResponseEvent $event) {
    if (!$event->isMasterRequest()) {
      return;
    }
    $response = $event->getResponse();
    if (\Drupal::state()->get('system.maintenance_mode')) {
      $response->headers->set('maintenance-mode', 'on');
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond', -100];
    return $events;
  }

}
