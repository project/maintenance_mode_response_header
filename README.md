# Maintenance mode response header

This module will add a reponse header to sites in maintenance mode.

```
maintenance-mode: on
```

It can be used for applications monitoring the downtime of the website to know if the site is down because of a failure, or because of maintenance.

## Usage

Install as usual, no additional configuration is required.
